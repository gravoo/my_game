#include <setup_opengl.hpp>
#include <shader_compiler.hpp>
#include <camera.hpp>
#include <game.hpp>
#include <input_handler.hpp>
#include <tuple>

void keyboard_button_call_back(GLFWwindow *window, int key, int, int action, int);

std::tuple<GLfloat, GLfloat> get_delta_time(GLfloat last_time);

user_interface::InputHandler input_handler{};

int main()
{
    auto window = initialize_glfw();
    glfwMakeContextCurrent(window);
    load_glad();
    setup_gl();
    glfwSetKeyCallback(window, keyboard_button_call_back);

    Graphic::ShaderCompiler shader_compiler{"../../shaders/vertex_shader.vert", "../../shaders/fragment_shader.frag"};
    Game game{shader_compiler.compile_shaders(), input_handler};
    auto delta_time{0.0f}, last_time{0.0f};
    while (!glfwWindowShouldClose(window))
    {
        std::tie(delta_time, last_time) = get_delta_time(last_time);
        glfwPollEvents();
        game.update(delta_time);
        glClearColor(0.2f, 0.3f, 0.4f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        game.render();
        glfwSwapBuffers(window);
    }

    glfwTerminate();
}

void keyboard_button_call_back(GLFWwindow *window, int key, int, int action, int)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

    if (key >= 0 && key < 300)
    {
        if (action == GLFW_PRESS)
        {
            input_handler.set_key(key);
        }
        else if (action == GLFW_RELEASE)
        {
            input_handler.unset_key(key);
        }
    }
}

std::tuple<GLfloat, GLfloat> get_delta_time(GLfloat last_time)
{
    auto current_time = glfwGetTime();
    auto delta_time = current_time - last_time;
    return std::make_tuple(delta_time, current_time);
}

