#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>

const GLuint SCREEN_WIDTH = 800;
const GLuint SCREEN_HEIGHT = 600;

GLFWwindow* initialize_glfw();
void setup_gl();
void load_glad();
