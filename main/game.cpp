#include "game.hpp"
#include <input_handler.hpp>
#include <hero_factory.hpp>
#include <main_menu.hpp>
#include <main_menu_input_component.hpp>
#include <game_stage.hpp>

Game::Game(Graphic::Shader &&shader, user_interface::InputHandler &input_handler) :
        my_shader(shader), sprite_renderer(my_shader),
        game_levels(std::make_unique<game_level::GameLevel>(game_level::MainMenu{std::make_unique<input_components::MenuInputComponent>(input_handler)},
                                                            input_handler, game_camera)), my_input_handler(input_handler)
{
    my_shader.use();
    my_shader.setUniformInteger("image", 0);
}

void Game::render()
{
    my_shader.use();
    my_shader.setUniformMatrix4("view", game_camera.get_matrix_view());
    my_shader.setUniformMatrix4("projection", game_camera.get_matrix_projection());
    game_levels->draw(sprite_renderer);
}

void Game::update(float delta_time)
{
    if (game_levels->update(delta_time) == game_level::GameLevelState::END)
    {
        game_levels = std::make_unique<game_level::GameLevel>(game_level::MainMenu{std::make_unique<input_components::MenuInputComponent>(my_input_handler)},
                                                              my_input_handler, game_camera);
    }
}

