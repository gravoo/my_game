#pragma once

#include <sprite_renderer.hpp>
#include <camera.hpp>
#include <shader.hpp>
#include <game_level.hpp>

namespace user_interface
{
    class InputHandler;
}

class Game
{
public:
    Game(Graphic::Shader &&shader, user_interface::InputHandler &input_handler);

    void update(float delta_time);

    void render();

private:
    user_interface::Camera game_camera{};
    Graphic::Shader my_shader;
    Graphic::SpriteRenderer sprite_renderer;
    std::unique_ptr<game_level::GameLevel> game_levels;
    user_interface::InputHandler &my_input_handler;
};
