#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <map>
#include <functional>

namespace user_interface
{

    class Camera
    {
    public:
        Camera();

        glm::mat4 get_matrix_view();

        glm::mat4 get_matrix_projection() const;

        void update_camera_position(const glm::vec3 change_position);

        void prepare_game_camera();

        void prepare_menu_camera();

    private:
        glm::vec3 Position{glm::vec3(0.0f, 0.0f, 3.0f)};
        glm::vec3 Front{glm::vec3(0.0f, 0.0f, -3.0f)};
        glm::vec3 WorldUp{glm::vec3(0.0f, 1.0f, 0.0f)};
        glm::vec3 Up;
        glm::vec3 Right;
    };

}
