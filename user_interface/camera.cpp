#include "camera.hpp"

namespace user_interface
{
    glm::mat4 Camera::get_matrix_view()
    {
        return glm::lookAt(Position, Position + Front, Up);
    }

    glm::mat4 Camera::get_matrix_projection() const
    {
        return glm::ortho(0.0f, 800.0f, 600.0f, 0.0f, -1.0f, 5.0f);
    }

    Camera::Camera()
    {
//        Front = glm::normalize(Front);
        Right = glm::normalize(glm::cross(Front, WorldUp));
        Up = glm::normalize(glm::cross(Right, Front));
    }

    void Camera::update_camera_position(const glm::vec3 change_position)
    {
        Position += change_position;
    }

    void Camera::prepare_game_camera()
    {
        Position = glm::vec3(330.0f, 200.0f, 3.0f);
    }

    void Camera::prepare_menu_camera()
    {
        Position = glm::vec3(0.0f, 0.0f, 3.0f);
    }

}

