#include "camera_position_update.hpp"

namespace user_interface
{

CameraPositionUpdate::CameraPositionUpdate( Camera& camera )
    : camera( camera ) {}

void CameraPositionUpdate::update_camera_position( const glm::vec2 && change_in_position )
{
    camera.update_camera_position(glm::vec3(change_in_position, 0));
}

}

