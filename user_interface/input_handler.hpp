#pragma once
#include <array>

namespace user_interface
{

class InputHandler
{
public:
    InputHandler( );

    void set_key( int key );
    void unset_key( int key );
    std::array<bool, 300> get_key_array();

private:
    std::array<bool, 300> keys;
};
}
