#include "input_handler.hpp"

namespace user_interface
{

InputHandler::InputHandler()
{
    keys.fill( false );
}


void InputHandler::set_key( int key )
{
    keys[key] = true;
}


void InputHandler::unset_key( int key )
{
    keys[key] = false;
}


std::array< bool, 300 > InputHandler::get_key_array()
{
    return keys;
}
}
