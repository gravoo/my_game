cmake_minimum_required(VERSION 2.8.2)
add_compile_options(-Wall -Wextra -Wpedantic)
set(SOURCE camera.cpp input_handler.cpp camera_position_update.cpp)

add_subdirectory(tests)
add_library(user_interface STATIC ${SOURCE})
target_link_libraries(user_interface PRIVATE glm)
target_include_directories(user_interface PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
