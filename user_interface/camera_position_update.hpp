#pragma once
#include <camera.hpp>
#include <vector>

namespace user_interface
{
class CameraPositionUpdate
{
public:
    CameraPositionUpdate( Camera& camera );
    void update_camera_position(const glm::vec2 && change_in_position);
private:
    Camera& camera;
};
}
