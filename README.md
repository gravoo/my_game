# my_game

## Adventure, hack and slash game.

1. Dependency for ubuntu:bionic
 * apt-get install build-essential g++ cmake pkg-config libglfw3-dev libglew-dev libgl1-mesa-dev
2. Compile and run for
 * mkdir build
 * cd build
 * cmake ..
 * make
 * cd main
 * ./my_game
3. Keyboard layout
 * ![keyboard](https://gitlab.com/gravoo/my_game/raw/master/screen_shots/keyboard_layout.jpg)
 * <kbd>W</kbd> <kbd>S</kbd> <kbd>A</kbd> <kbd>D</kbd> : shoot direction
 * <kbd>&uarr;</kbd> <kbd>&larr;</kbd> <kbd>&darr;</kbd> <kbd>&rarr;</kbd> : move
 * <kbd>esc</kbd> : main menu
 * <kbd>enter</kbd> : apply
4. Screen shots:
 * ![screen_menu](https://gitlab.com/gravoo/my_game/raw/master/screen_shots/menu_screen.jpg)
 * ![screen_1](https://gitlab.com/gravoo/my_game/raw/master/screen_shots/screen_1.jpg)
 * ![screen_2](https://gitlab.com/gravoo/my_game/raw/master/screen_shots/screen_2.jpg)
 * ![screen_2](https://gitlab.com/gravoo/my_game/raw/master/screen_shots/screen_4.jpg)
 * ![screen_2](https://gitlab.com/gravoo/my_game/raw/master/screen_shots/screen_6.jpg)



