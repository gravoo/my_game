#include <main_menu.hpp>
#include <main_menu_input_component.hpp>
#include <sprite_renderer.hpp>
#include <sprite_factory.hpp>


namespace game_level
{
    MainMenu::MainMenu(std::unique_ptr<input_components::MenuInputComponent> input_component)
            : my_input_component(std::move(input_component))
    {
        menu_components.push_back(
                MenuEntryComponent{
                        Graphic::SpriteFactory().create_sprite("../../textures/new_game.png", GL_RGBA,
                                                               glm::vec2(250, 100), glm::vec2(256, 64))});
        menu_components.push_back(
                MenuEntryComponent{Graphic::SpriteFactory().create_sprite("../../textures/exit.png", GL_RGBA,
                                                                          glm::vec2(270, 200), glm::vec2(256, 64))});

        menu_components.push_back(
                MenuEntryComponent{Graphic::SpriteFactory().create_sprite("../../textures/keyboard_layout.png",
                                                                          GL_RGBA, glm::vec2(270, 400), glm::vec2(256, 64))});
        menu_components.push_back(
                MenuEntryComponent{Graphic::SpriteFactory().create_sprite("../../textures/pointing_arrow.png",
                                                                          GL_RGBA, glm::vec2(50, 100), glm::vec2(256, 64))});
    }

    void MainMenu::draw(Graphic::SpriteRenderer &sprite_renderer)
    {
        for (auto &menu_entry : menu_components)
        {
            menu_entry.draw(sprite_renderer);
        }
    }


    GameState MainMenu::update(float delta_time)
    {
        auto user_input = my_input_component->update();
        if (user_input == input_components::MenuInput::DOWN)
        {
            menu_components.back().set_sprite_position(glm::vec2(50, 200));
            menu_state = GameState::EXIT;
        }
        else if (user_input == input_components::MenuInput::UP)
        {
            menu_components.back().set_sprite_position(glm::vec2(50, 100));
            menu_state = GameState::START;
        }
        else if (user_input == input_components::MenuInput::ENTER)
        {
            return GameState::START;
        }
        return GameState::MENU;

    }

    GameState MainMenu::get_menu_state()
    {
        return menu_state;
    }

}

