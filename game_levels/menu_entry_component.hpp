#pragma once
#include <sprite_data.hpp>

namespace Graphic
{
class SpriteRenderer;
}

namespace game_level
{
class MenuEntryComponent
{
public:
    MenuEntryComponent(Graphic::SpriteData sprite);
    void update(float delta_time);
    void draw(Graphic::SpriteRenderer& sprite_renderer);
    void set_sprite_position(glm::vec2 new_sprite_position);
private:
    Graphic::SpriteData my_sprite;

};

}

