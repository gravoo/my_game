#pragma once

#include <string>
#include <sprite_data.hpp>
#include <map>
#include <memory>
#include <set>
#include <world_map.hpp>
#include <hero_factory.hpp>
#include "main_menu.hpp"
#include "main_hero_builder.hpp"
#include "game_map_frame_builder.hpp"
#include <enemy_builder.hpp>

namespace my_hero
{
    class IHero;
}
namespace Graphic
{
    class SpriteRenderer;
}

namespace game_level
{
    class GameStage
    {
    public:
        GameStage(EnemyBuilder enemy_builder, MainHeroBuilder main_hero, GameMapFrameBuilder game_map_frames_builder);

        void draw(Graphic::SpriteRenderer &sprite_renderer);

        GameState update(float delta_time);

    private:
        std::set<std::unique_ptr<my_hero::IHero>> heroes;
        game_world::WorldMap game_world;
        std::shared_ptr<physics::Coords> main_character;
        unsigned number_of_unmovable_objects{0};
    };
}
