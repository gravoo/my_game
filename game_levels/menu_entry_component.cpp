#include <menu_entry_component.hpp>
#include <sprite_renderer.hpp>

namespace game_level
{
MenuEntryComponent::MenuEntryComponent(Graphic::SpriteData sprite)
    : my_sprite(sprite) {}

void MenuEntryComponent::update(float delta_time)
{

}


void MenuEntryComponent::draw(Graphic::SpriteRenderer& sprite_renderer)
{
    sprite_renderer.draw_sprite(my_sprite);
}


void MenuEntryComponent::set_sprite_position(glm::vec2 new_sprite_position)
{
    my_sprite.position = new_sprite_position;
}

}

