#pragma once

#include <string>
#include <sprite_data.hpp>
#include <map>
#include <sprite_factory.hpp>
#include <hero_factory.hpp>

namespace game_level
{
    using MainHeroSprites = std::map<std::string, Graphic::SpriteData>;
    using HeroesSet = std::set<std::unique_ptr<my_hero::IHero>>;

    class MainHeroBuilder
    {
    public:
        MainHeroBuilder(my_hero::HeroFactory hero_factory);

        void prepare_main_hero(HeroesSet &heroes, std::shared_ptr<physics::Coords> main_character);

    private:
        MainHeroSprites sprites;
        my_hero::HeroFactory my_hero_factory;

    };

}

