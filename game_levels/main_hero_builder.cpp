#include <hero_factory.hpp>
#include "main_hero_builder.hpp"

game_level::MainHeroBuilder::MainHeroBuilder(my_hero::HeroFactory hero_factory) : my_hero_factory(std::move(hero_factory))
{
    sprites["king1"] = Graphic::SpriteFactory().create_sprite("../../textures/warior_king/king_1.png", GL_RGBA);
    sprites["king2"] = Graphic::SpriteFactory().create_sprite("../../textures/warior_king/king_2.png", GL_RGBA);
    sprites["king3"] = Graphic::SpriteFactory().create_sprite("../../textures/warior_king/king_3.png", GL_RGBA);
    sprites["king4"] = Graphic::SpriteFactory().create_sprite("../../textures/warior_king/king_4.png", GL_RGBA);
    sprites["king5"] = Graphic::SpriteFactory().create_sprite("../../textures/warior_king/king_5.png", GL_RGBA);
    sprites["king6"] = Graphic::SpriteFactory().create_sprite("../../textures/warior_king/king_6.png", GL_RGBA);
}

void game_level::MainHeroBuilder::prepare_main_hero(game_level::HeroesSet &heroes, std::shared_ptr<physics::Coords> main_character)
{
    my_hero_factory.create_and_insert_hero(main_character,
                                           {sprites["king1"], sprites["king2"], sprites["king3"],
                                            sprites["king4"], sprites["king5"], sprites["king6"],
                                            sprites["king5"], sprites["king3"], sprites["king3"],
                                            sprites["king2"], sprites["king1"]}, heroes);
}
