#include <sprite_factory.hpp>
#include "enemy_builder.hpp"


game_level::EnemyBuilder::EnemyBuilder(std::initializer_list<glm::vec2> enemies_positions) : my_enemies_positions(enemies_positions)
{
    sprites["enemy_face1"] = Graphic::SpriteFactory().create_sprite("../../textures/enemy_face1.png", GL_RGBA);
    sprites["enemy_face2"] = Graphic::SpriteFactory().create_sprite("../../textures/enemy_face2.png", GL_RGBA);
    sprites["enemy_face3"] = Graphic::SpriteFactory().create_sprite("../../textures/enemy_face3.png", GL_RGBA);
    sprites["enemy_face4"] = Graphic::SpriteFactory().create_sprite("../../textures/enemy_face4.png", GL_RGBA);
}

void game_level::EnemyBuilder::prepare_hero_enemies(std::set<std::unique_ptr<my_hero::IHero>> &heroes, std::shared_ptr<physics::Coords> main_character)
{
    for (auto enemy_position : my_enemies_positions)
    {
        enemy_factory.create_and_insert_enemy({sprites["enemy_face1"], sprites["enemy_face2"],
                                               sprites["enemy_face3"], sprites["enemy_face4"],
                                               sprites["enemy_face3"], sprites["enemy_face2"]},
                                              physics::Coords{enemy_position, glm::vec2(40, 40)}, 1,
                                              heroes, main_character);
    }
}

game_level::EnemieSprites game_level::EnemyBuilder::get_sprites()
{
    return sprites;
}

