#pragma once

#include <map_frames_factory.hpp>
#include <algorithm>
#include <sprite_data.hpp>
#include <map>
#include <sprite_factory.hpp>

namespace my_hero
{
    class IHero;
}
namespace game_level
{
    class GameMapFrameBuilder
    {
    public:
        GameMapFrameBuilder(std::initializer_list<physics::Coords> short_walls, std::initializer_list<physics::Coords> long_walls);

        size_t prepare_level_frame(std::set<std::unique_ptr<my_hero::IHero>> &heroes);

    private:
        game_world::MapFramesFactory my_frame_factory;
        std::initializer_list<physics::Coords> short_walls;
        std::initializer_list<physics::Coords> long_walls;
    };
}



