#include "game_stage.hpp"
#include "main_menu.hpp"
#include "main_hero_builder.hpp"
#include "map_frames_factory.hpp"
#include "game_map_frame_builder.hpp"
#include <i_hero.hpp>
#include <sprite_renderer.hpp>

namespace game_level
{
    GameStage::GameStage(EnemyBuilder enemy_builder, MainHeroBuilder main_hero, GameMapFrameBuilder game_map_frames_builder)
            : main_character(std::make_shared<physics::Coords>(physics::Coords{glm::vec2(700, 500), glm::vec2(40, 40)}))
    {
        number_of_unmovable_objects = game_map_frames_builder.prepare_level_frame(heroes);
        main_hero.prepare_main_hero(heroes, main_character);
        enemy_builder.prepare_hero_enemies(heroes, main_character);
        game_world.build_simple_map();
    }

    void GameStage::draw(Graphic::SpriteRenderer &sprite_renderer)
    {
        for (auto const &hero : heroes)
        {
            hero->draw(sprite_renderer);
        };
        game_world.draw(sprite_renderer);
    }


    GameState GameStage::update(float delta_time)
    {
        for (auto const &hero : heroes)
        {
            hero->update(delta_time);

            if (hero->get_hero_state() == my_hero::HeroState::inactive)
            {
                heroes.erase(hero);
            }
            else if (hero->get_hero_state() == my_hero::HeroState::end)
            {
                return GameState::FINISHED;
            }
        }
        if (heroes.size() == number_of_unmovable_objects + 1)
        {
            return GameState::COMPLETED;
        }
        return GameState::ACTIVE;
    }


}

