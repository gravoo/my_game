#pragma once

#include <memory>
#include <map>
#include <string>
#include <vector>
#include <menu_entry_component.hpp>

namespace input_components
{
    class MenuInputComponent;
}

namespace Graphic
{
    class SpriteRenderer;
}

namespace game_level
{
    enum class GameState
    {
        START, MENU, ACTIVE, COMPLETED, EXIT, FINISHED
    };

    class MainMenu
    {
    public:
        MainMenu(std::unique_ptr<input_components::MenuInputComponent> input_component);

        GameState update(float delta_time);

        void draw(Graphic::SpriteRenderer &sprite_renderer);

        GameState get_menu_state();

    private:
        std::unique_ptr<input_components::MenuInputComponent> my_input_component;
        std::vector<MenuEntryComponent> menu_components;
        GameState menu_state{GameState::START};
    };

}
