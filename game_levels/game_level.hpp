#pragma once

#include <main_menu.hpp>
#include <main_menu_input_component.hpp>
#include <game_stage.hpp>
#include <memory>
#include <vector>

namespace Graphic
{
    class SpriteRenderer;
}

namespace game_level
{
    enum class GameLevelState
    {
        RUN, END
    };

    class GameLevel
    {
    public:
        GameLevel(MainMenu main_menu,
                  user_interface::InputHandler &input_handler,
                  user_interface::Camera &game_camera);

        GameLevelState update(float delta_time);

        void draw(Graphic::SpriteRenderer &sprite_renderer);

    private:
        MainMenu my_main_menu;
        std::vector<std::unique_ptr<GameStage>> default_game_stage;
        GameState game_current_state{GameState::MENU};
        user_interface::Camera &game_camera;

    };

}
