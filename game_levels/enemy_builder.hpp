#pragma once

#include <string>
#include <sprite_data.hpp>
#include <map>
#include <coordinate_data.hpp>
#include <i_hero.hpp>
#include <memory>
#include <set>
#include <hero_factory.hpp>
#include <enemy_factory.hpp>

namespace game_level
{
    using EnemieSprites = std::map<std::string, Graphic::SpriteData>;

    class EnemyBuilder
    {
    public:
        EnemyBuilder(std::initializer_list<glm::vec2> enemies_positions);

        void prepare_hero_enemies(std::set<std::unique_ptr<my_hero::IHero>> &heroes, std::shared_ptr<physics::Coords> main_character);

        EnemieSprites get_sprites();

    private:

        EnemieSprites sprites;
        std::shared_ptr<physics::Coords> main_character;
        my_hero::EnemyFactory enemy_factory;
        std::initializer_list<glm::vec2> my_enemies_positions;
    };
}

