#include <game_level.hpp>
#include <sprite_renderer.hpp>
#include <cstdlib>
#include <iostream>
#include "enemy_builder.hpp"
#include <game_map_frame_builder.hpp>

namespace game_level
{
    GameLevel::GameLevel(MainMenu main_menu,
                         user_interface::InputHandler &input_handler,
                         user_interface::Camera &game_camera)
            : my_main_menu(std::move(main_menu)), game_camera(game_camera)
    {
        game_camera.prepare_menu_camera();
        default_game_stage.emplace_back(
                std::make_unique<game_level::GameStage>(
                        EnemyBuilder{glm::vec2(100, 400), glm::vec2(150, 450), glm::vec2(700, 100), glm::vec2(600, 200), glm::vec2(100, 200),
                                     glm::vec2(100, 250)},
                        MainHeroBuilder{my_hero::HeroFactory{game_camera, input_handler}},
                        GameMapFrameBuilder{std::initializer_list<physics::Coords>{
                                physics::Coords{glm::vec2(400, 100), glm::vec2(40, 500)}},
                                            std::initializer_list<physics::Coords>{
                                                    physics::Coords{glm::vec2(100, 300), glm::vec2(600, 40)}}}));
        default_game_stage.emplace_back(
                std::make_unique<game_level::GameStage>(
                        EnemyBuilder{glm::vec2(200, 350), glm::vec2(200, 100), glm::vec2(100, 50), glm::vec2(150, 150), glm::vec2(700, 150)},
                        MainHeroBuilder{my_hero::HeroFactory{game_camera, input_handler}},
                        GameMapFrameBuilder{std::initializer_list<physics::Coords>{},
                                            std::initializer_list<physics::Coords>{
                                                    physics::Coords{glm::vec2(100, 450), glm::vec2(700, 40)},
                                                    physics::Coords{glm::vec2(0, 300), glm::vec2(700, 40)}}}));
        default_game_stage.
                emplace_back(
                std::make_unique<game_level::GameStage>(EnemyBuilder{glm::vec2(100, 100)}, MainHeroBuilder{my_hero::HeroFactory{game_camera, input_handler}},
                                                        GameMapFrameBuilder{std::initializer_list<physics::Coords>{
                                                                physics::Coords{glm::vec2(600, 400), glm::vec2(40, 100)}},
                                                                            std::initializer_list<physics::Coords>{
                                                                                    physics::Coords{glm::vec2(600, 400), glm::vec2(200, 50)}}}));
    }

    void GameLevel::draw(Graphic::SpriteRenderer &sprite_renderer)
    {
        if (game_current_state == GameState::MENU)
        {
            my_main_menu.draw(sprite_renderer);
        }
        else if (game_current_state == GameState::START)
        {
            default_game_stage.back()->draw(sprite_renderer);
        }
        else if (game_current_state == GameState::EXIT)
        {
            std::exit(EXIT_SUCCESS);
        }
    }


    GameLevelState GameLevel::update(float delta_time)
    {
        if (game_current_state == GameState::MENU)
        {
            auto menu_status = my_main_menu.update(delta_time);

            if (menu_status == GameState::START)
            {
                auto game_status = my_main_menu.get_menu_state();

                if (game_status == GameState::START)
                {
                    game_camera.prepare_game_camera();
                    game_current_state = GameState::START;
                }
                else if (game_status == GameState::EXIT)
                {
                    game_current_state = GameState::EXIT;
                }
            }
        }
        else if (game_current_state == GameState::START)
        {
            auto stage_state = default_game_stage.back()->update(delta_time);
            if (stage_state == GameState::COMPLETED && !default_game_stage.empty())
            {
                default_game_stage.pop_back();
                game_camera.prepare_game_camera();
                std::cerr << "STAGE COMPLETED \n";
            }
            else if (stage_state == GameState::FINISHED)
            {
                std::cerr << "Main Hero is dead, going back to menu entry \n";
                return GameLevelState::END;
            }
        }
        if (default_game_stage.empty())
        {
            std::cerr << "GAME FINISHED, CONGRATS!!! \n";
            return GameLevelState::END;
        }
        return GameLevelState::RUN;
    }

}

