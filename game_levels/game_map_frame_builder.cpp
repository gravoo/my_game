#include "game_map_frame_builder.hpp"
#include <i_hero.hpp>

size_t game_level::GameMapFrameBuilder::prepare_level_frame(std::set<std::unique_ptr<my_hero::IHero>> &heroes)
{
    std::map<std::string, Graphic::SpriteData> sprites;
    sprites["frame_longest_side"] = Graphic::SpriteFactory().create_sprite("../../textures/frame_longest_side.png", GL_RGBA);
    sprites["frame_shortest_side"] = Graphic::SpriteFactory().create_sprite("../../textures/frame_shortest_side.png", GL_RGBA);
    my_frame_factory.create_and_insert_unmovable_hero({sprites["frame_longest_side"]},
                                                      physics::Coords{glm::vec2(0, 0), glm::vec2(800, 40)}, heroes);
    my_frame_factory.create_and_insert_unmovable_hero({sprites["frame_longest_side"]},
                                                      physics::Coords{glm::vec2(0, 560), glm::vec2(800, 40)}, heroes);
    my_frame_factory.create_and_insert_unmovable_hero({sprites["frame_shortest_side"]},
                                                      physics::Coords{glm::vec2(0, 0), glm::vec2(40, 600)}, heroes);
    my_frame_factory.create_and_insert_unmovable_hero({sprites["frame_shortest_side"]},
                                                      physics::Coords{glm::vec2(760, 0), glm::vec2(40, 600)}, heroes);
    auto unmovable_objects{4};
    for (auto walls : short_walls)
    {
        unmovable_objects++;
        my_frame_factory.create_and_insert_unmovable_hero({sprites["frame_shortest_side"]},
                                                          physics::Coords{walls.position, walls.size}, heroes);

    }
    for (auto walls : long_walls)
    {
        unmovable_objects++;
        my_frame_factory.create_and_insert_unmovable_hero({sprites["frame_longest_side"]},
                                                          physics::Coords{walls.position, walls.size}, heroes);
    }
    return unmovable_objects;
}


game_level::GameMapFrameBuilder::GameMapFrameBuilder(std::initializer_list<physics::Coords>
                                                     short_walls, std::initializer_list<physics::Coords>
                                                     long_walls)
        : short_walls(short_walls), long_walls(long_walls)
{}

