#pragma once
#include <glm/glm.hpp>
#include <enemy_guard_component.hpp>
#include <arrow_component.hpp>
#include <arrow_factory.hpp>
#include <i_hero.hpp>
#include <i_physics_component.hpp>

namespace my_enemy
{
class AiComponent
{
public:
    AiComponent(unsigned int sight_range, my_hero::ArrowComponent arrow_component);
    glm::vec2 udate(const glm::vec2 & protagonist_location,
                    physics::Coords &enemy_coords, int speed, float delta_time);
    glm::vec2 update_position(const glm::vec2 & protagonist_location, const glm::vec2 & enemy_location, int speed);
    bool hero_in_range(const glm::vec2 & protagonist_location, const glm::vec2 & enemy_location);
    void draw(Graphic::SpriteRenderer& sprite_renderer);
private:
    unsigned int sight_range;
    EnemyGuardComponent guard_behaviour;
    my_hero::ArrowComponent arrow_component;
};
}
