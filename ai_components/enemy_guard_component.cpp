#include <enemy_guard_component.hpp>
#include <position_updater.hpp>


namespace my_enemy
{
glm::vec2 EnemyGuardComponent::update_x_position( int speed )
{
    glm::vec2 step {0};
    step.x += speed;
    return step;
}

glm::vec2 EnemyGuardComponent::guard_area()
{
    if( num_of_steps >= guard_range )
    {
        num_of_steps = 0;
        speed *= -1;
    }

    ++num_of_steps;
    return update_x_position( speed );
}

}

