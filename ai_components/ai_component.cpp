#include <ai_component.hpp>

namespace my_enemy
{
AiComponent::AiComponent( unsigned int sight_range, my_hero::ArrowComponent arrow_component ) : sight_range( sight_range ) ,
    guard_behaviour( sight_range ), arrow_component(std::move(arrow_component)) {}


glm::vec2 AiComponent::update_position( const glm::vec2& protagonist_location, const glm::vec2& enemy_location, int speed )
{
    glm::vec2 step {0};

    if( protagonist_location.x < enemy_location.x )
    {
        step.x -= speed;
        step = glm::normalize(step);
    }
    else if( protagonist_location.x > enemy_location.x )
    {
        step.x += speed;
        step = glm::normalize(step);
    }

    if( protagonist_location.y < enemy_location.y )
    {
        step.y -= speed;
        step = glm::normalize(step);
    }
    else if( protagonist_location.y > enemy_location.y )
    {
        step.y += speed;
        step = glm::normalize(step);
    }

    return step;
}


bool AiComponent::hero_in_range( const glm::vec2& protagonist_location, const glm::vec2& enemy_location )
{
    if( glm::distance( protagonist_location, enemy_location) < sight_range &&
            glm::distance( protagonist_location, enemy_location) >= 100 )
    {
        return true;
    }

    return false;
}


glm::vec2 AiComponent::udate( const glm::vec2& protagonist_location,
                              physics::Coords& enemy_coords, int speed, float delta_time)
{
    glm::vec2 step {0};

    if( hero_in_range( protagonist_location, enemy_coords.position ) )
    {
        arrow_component.update(delta_time, glm::normalize(protagonist_location - enemy_coords.position),
                               enemy_coords, glm::vec2(speed, speed));
        step = update_position( protagonist_location, enemy_coords.position, speed );
    }
    else
    {
        arrow_component.update(delta_time, glm::vec2{0}, enemy_coords, glm::vec2(speed, speed));
        step = guard_behaviour.guard_area();
    }

    return step;
}
void AiComponent::draw(Graphic::SpriteRenderer& sprite_renderer)
{
    arrow_component.draw_arrows(sprite_renderer);
}

}

