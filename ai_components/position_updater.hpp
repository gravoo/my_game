#pragma once
#include <glm/glm.hpp>

namespace my_enemy
{

class PositionUpdater
{
public:
    PositionUpdater();
    static glm::vec2 update_x_position( int speed )
    {
        glm::vec2 step {0};
        step.x += speed;
        return step;
    }
    static glm::vec2 update_y_position( int speed )
    {
        glm::vec2 step {0};
        step.y += speed;
        return step;
    }
};
}
