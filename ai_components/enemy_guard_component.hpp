#pragma once
#include <glm/glm.hpp>


namespace my_enemy
{
class EnemyGuardComponent
{
public:
    EnemyGuardComponent( unsigned int guard_range ) : guard_range( guard_range ) {};
    glm::vec2 guard_area( );
private:
    glm::vec2 update_x_position( int speed );
    unsigned int guard_range;
    unsigned int num_of_steps {0};
    int speed {1};
};
}
