// #include <gtest/gtest.h>
// #include <ai_component.hpp>
// #include <iostream>
//
// using namespace testing;
// using namespace my_enemy;
//
// ::std::ostream& operator<<(::std::ostream& os, const glm::vec2 & values) {
//   return os <<"x: "<< values.x << " y: "<<values.y;
// }
//
// struct ai_components_ut : public Test
// {
//     glm::vec2 protagonist_location{0, 0};
//     glm::vec2 enemy_location{5, 5};
//     AiComponent ai_component{6};
//     int speed{1};
// };
//
// TEST_F( ai_components_ut, get_closert_to_protagonist )
// {
//     auto step = ai_component.update_position(protagonist_location, enemy_location, speed);
//     ASSERT_EQ(glm::vec2(-1, -1), step);
// }
//
// TEST_F( ai_components_ut, get_closert_to_protagonist_reverse )
// {
//     enemy_location = glm::vec2{0, 0};
//     protagonist_location =  glm::vec2 {5, 5};
//     auto step = ai_component.update_position(protagonist_location, enemy_location, speed);
//     ASSERT_EQ(glm::vec2(1, 1), step);
// }
//
// TEST_F( ai_components_ut, detect_protagonist_in_enemy_range )
// {
//     ASSERT_TRUE(ai_component.hero_in_range(protagonist_location, enemy_location));
// }
//
// TEST_F( ai_components_ut, protagonist_is_not_detectable_by_enemy )
// {
//     enemy_location+=speed;
//     ASSERT_FALSE(ai_component.hero_in_range(protagonist_location, enemy_location));
// }
//
// TEST_F( ai_components_ut, detect_protagonist_in_enemy_range_make_step_toward_hero )
// {
//     ASSERT_EQ(glm::vec2(-1, -1), ai_component.udate(protagonist_location, enemy_location, speed));
// }
//
// TEST_F( ai_components_ut, guard_area_if_no_hero_nerby )
// {
//     enemy_location+=speed;
//     ASSERT_EQ(glm::vec2(1, 0), ai_component.udate(protagonist_location, enemy_location, speed));
// }
