#include <gtest/gtest.h>
#include <enemy_guard_component.hpp>
#include <iostream>

using namespace testing;
using namespace my_enemy;

struct EnemyGuardComponentUt : public Test
{
    EnemyGuardComponent guard_component{5};
    glm::vec2 enemy_location{5, 5};
    int speed{1};

};

TEST_F(EnemyGuardComponentUt, make_n_steps_right_and_go_back)
{
    for(auto n{0}; n<10; ++n)
    {
        enemy_location+=guard_component.guard_area();
    }
    ASSERT_EQ(glm::vec2(5, 5), enemy_location);
}
