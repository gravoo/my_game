# Project status

- [x] Setup repo
- [x] Setup ci
    * during setup of yam, gtest and docker to work properly
- [x] Prepare project structure and setup tests
- [x] Add graphic library
    * graphic library added
- [x] Adding glad library for handling OS specific function
    * during getting know cmake
- [x] CI Job align with graphic libraries
- [x] make use of cmake and splitted code
- [x] find first step
- [x] remove unnecesary lib and go back to opengl
- [x] add shader compile class
    * remember, never return pointer to temporary object from function
- [x] make 2D camera view
- [x] encapsulate game
- [x] update yml file
- [x] implement collision detection mechanism
    * do not update camera after colision - done
    * figure out clever way to pass sprite data to hero class
- [x] separate enemy from hero, introduce common interface
- [x] move camera_updater to hero class
- [x] prepare simple game map
    * update camera to center of hero sprite
- [x] let enemy find hero
    * add guar option
    * clean code
    * removed ugly frames
- [x] introduce animation component, make more enemies
    * time for code cleaning
    * when trying to change design, do it on branch
- [x] basic arrow attack for hero
- [x] basic system for receiving damage
    * make arrowComponent manage arrows
- [x] when hero in range enemies can shoot too
- [x] code cleaning
- [x] prepare main menu
- [x] hp bars for characters
- [x] add more game levels

