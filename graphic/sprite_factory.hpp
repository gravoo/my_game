#pragma once
#include <sprite_data.hpp>
#include <glad/glad.h>
#include <texture_loader.hpp>
#include <string>

namespace Graphic
{
class SpriteFactory
{
public:
    SpriteFactory() = default;
    static SpriteData create_sprite(std::string texture_path, GLenum texture_format,
                             glm::vec2 position =  glm::vec2( 0, 0 ), glm::vec2 size = glm::vec2( 40, 40 ))
    {
        return SpriteData{TextureLoader().generate_texture( texture_path , texture_format), position, size};
    }
};
}

