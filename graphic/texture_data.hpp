#pragma once
#include <glad/glad.h>
namespace Graphic
{
struct TextureData
{
    GLuint get_texture_id() const
    {
        return texture_id;
    }
    void bind_texture() const
    {
        glBindTexture( GL_TEXTURE_2D, texture_id );
    }
    GLuint texture_id;
    GLsizei number_of_texture_objects;
    GLint width, height, nrChannels;
    GLenum texture_format;
};
}
