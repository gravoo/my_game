#include <sprite_animator.hpp>


namespace Graphic
{
    void SpriteAnimator::draw_sprite(SpriteRenderer &sprite_renderer, const physics::Coords &coords)
    {
        sprite_renderer.draw_sprite(sprites[curent_sprite], coords);
    }

    void SpriteAnimator::update(float delta_time)
    {
        current_frame += delta_time;

        if (current_frame > fps)
        {
            curent_sprite = (curent_sprite + 1) % sprites.size();
            current_frame = 0;
        }
    }

    SpriteAnimator::SpriteAnimator(std::initializer_list<SpriteData> sprite_list)
            : sprites(sprite_list)
    {
        fps = 1.0f / static_cast<float>(sprites.size());
    };

}

