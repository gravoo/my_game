#include "texture_loader.hpp"
#include <tuple>
#include <iostream>
#include <stb_image.h>
namespace
{
std::tuple<unsigned char*, GLint, GLint, GLint> load_texture_from_file( std::string relative_path_to_texture )
{
    GLint width, height, nrChannels;
    unsigned char* data = stbi_load( relative_path_to_texture.c_str(), &width, &height, &nrChannels, 0 );

    if( !data )
    {
        throw std::runtime_error( "ERROR::CAN'T LOAD TEXTURE FROM FILE" );
    }

    return std::make_tuple( data, width, height, nrChannels );
}

}

namespace Graphic
{

TextureData TextureLoader::generate_texture( std::string relative_path_to_texture, GLenum texture_format )
{
    GLsizei number_of_texture_objects {1};
    GLuint texture_id;
    glGenTextures( number_of_texture_objects, &texture_id );
    glBindTexture( GL_TEXTURE_2D, texture_id );
    auto [data, width, height, nrChannels] = load_texture_from_file( relative_path_to_texture );
    glTexImage2D( GL_TEXTURE_2D, 0, texture_format, width, height, 0, texture_format, GL_UNSIGNED_BYTE, data );
    glGenerateMipmap( GL_TEXTURE_2D );
    glBindTexture( GL_TEXTURE_2D, 0 );
    stbi_image_free( data );
    return TextureData{texture_id, number_of_texture_objects, width, height, nrChannels, texture_format};
}

}
