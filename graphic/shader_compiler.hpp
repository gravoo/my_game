#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <shader.hpp>

namespace Graphic
{

class ShaderCompiler
{
public:
    ShaderCompiler ( std::string vertex_shader_path, std::string fragment_shader_path );
    Shader compile_shaders();
private:
    void link_shaders();
    void compile_vertex_shader ( );
    void compile_fragment_shader ( );
    unsigned int vertex_shader {0};
    unsigned int fragment_shader {0};
    unsigned int shader_id {0};
    std::string vertex_shader_source;
    std::string fragment_shader_source;
};

}
