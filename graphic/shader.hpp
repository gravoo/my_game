#pragma once
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <string>


namespace Graphic
{
class Shader
{
public:
    Shader(unsigned int shader_id);
    void  use() const;
    void  setUniformInteger( std::string name, float value ) const;
    void  setUniformMatrix4( std::string name, glm::mat4 matrix ) const;
    void  setUniformVector3f( std::string name, glm::vec3 vector ) const;
private:
    unsigned int shader_id {0};
};

}
