#include <sprite_renderer.hpp>
#include <cube.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <rectangle.hpp>


namespace Graphic
{

void SpriteRenderer::draw_sprite( const SpriteData& sprite_data )
{
    set_sprite_model( sprite_data.position, sprite_data.size, sprite_data.rotate );
    set_sprite_color( sprite_data.color );
    bind_texture( sprite_data.texture_data );
    draw_arrays();
}

void SpriteRenderer::draw_sprite( const SpriteData& sprite_data ,
                                  const physics::Coords& coords)
{
    set_sprite_model( coords.position, coords.size, coords.rotate );
    set_sprite_color( sprite_data.color );
    bind_texture( sprite_data.texture_data );
    draw_arrays();
}

void SpriteRenderer::draw_arrays()
{
    glBindVertexArray( VAO );
    glDrawArrays( GL_TRIANGLES, 0, 6 );
    glBindVertexArray( 0 );
}
SpriteRenderer::~SpriteRenderer()
{
    glDeleteVertexArrays( 1, &VAO );
    glDeleteBuffers( 1, &VBO );
}

SpriteRenderer::SpriteRenderer( const Shader& shader ):
    shader( shader )
{
    glGenVertexArrays( num_of_buffer_objects, &VAO );
    glGenBuffers( num_of_buffer_objects, &VBO );

    glBindBuffer( GL_ARRAY_BUFFER, VBO );
    glBufferData( GL_ARRAY_BUFFER, sizeof( rectangle::vertices ), rectangle::vertices, GL_STATIC_DRAW );

    glBindVertexArray( VAO );
    glEnableVertexAttribArray( 0 );
    glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof( float ), ( void* )0 );

    glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindVertexArray( 0 );
}


void SpriteRenderer::set_sprite_model( const glm::vec2 &position,
                                       const glm::vec2 & size,  float rotate )
{
    shader.use();
    glm::mat4 model {1.0f};
    model = glm::translate( model, glm::vec3 {position, 0} );
    model = glm::translate( model, glm::vec3( 0.5f * size.x, 0.5f * size.y, 0.0f ) );
    model = glm::rotate( model, rotate, glm::vec3( 0.0f, 0.0f, 1.0f ) );
    model = glm::translate( model, glm::vec3( -0.5f * size.x, -0.5f * size.y, 0.0f ) );
    model = glm::scale( model, glm::vec3( size, 1.0f ) );
    shader.setUniformMatrix4( "model", model );
}


void SpriteRenderer::bind_texture( const TextureData& texture )
{
    glActiveTexture( GL_TEXTURE0 );
    texture.bind_texture();
}


void SpriteRenderer::set_sprite_color( const glm::vec3 & color )
{
    shader.setUniformVector3f( "spriteColor", color );
}
}



