#pragma once
#include <glm/glm.hpp>

namespace physics
{

struct Coords
{
    glm::vec2 position {0};
    glm::vec2 size{40, 40};
    float rotate{0.0f};

    bool operator != (const Coords & rhs) const
    {
        return this->position != rhs.position;
    }
    float get_x_dimension() const
    {
        return position.x + size.x;
    }
    float get_y_dimension() const
    {
        return position.y + size.y;
    }

};
}
