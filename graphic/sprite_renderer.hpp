#pragma once
#include <glm/glm.hpp>
#include <shader.hpp>
#include <sprite_data.hpp>
#include <coordinate_data.hpp>

namespace Graphic
{

class SpriteRenderer
{
public:
    SpriteRenderer( const Shader &shader );
    void draw_sprite( const SpriteData &sprite_data);
    void draw_sprite( const SpriteData &sprite_data, const physics::Coords& coords);
    ~SpriteRenderer();
private:
    void set_sprite_model( const glm::vec2& position, const glm::vec2& size,  float rotate );
    void set_sprite_color(const glm::vec3& color);
    void bind_texture(const TextureData& texture);
    void draw_arrays();
    GLsizei num_of_buffer_objects {1};
    const Shader &shader;
    GLuint VBO, VAO;
};
}
