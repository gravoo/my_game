#include "shader.hpp"
#include <glm/gtc/type_ptr.hpp>

namespace Graphic
{

Shader::Shader(unsigned int shader_id) : shader_id(shader_id)
{
}

void Shader::use() const
{
    glUseProgram( shader_id );
}

void Shader::setUniformInteger( std ::string name, float value ) const
{
    glUniform1i( glGetUniformLocation( shader_id, name.c_str() ), value );
}


void Shader::setUniformVector3f( std ::string name, glm::vec3 vector ) const
{
    glUniform3f( glGetUniformLocation( shader_id, name.c_str() ), vector.x, vector.y, vector.z );
}


void Shader::setUniformMatrix4( std ::string name, glm::mat4 matrix ) const
{
    glUniformMatrix4fv( glGetUniformLocation( shader_id, name.c_str() ), 1, false, glm::value_ptr( matrix ) );
}

}

