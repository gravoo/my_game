#pragma once

#include <sprite_data.hpp>
#include <sprite_renderer.hpp>
#include <vector>
#include <initializer_list>
#include <functional>
#include <glm/glm.hpp>

namespace Graphic
{
    class SpriteAnimator
    {
    public:
        SpriteAnimator(std::initializer_list<SpriteData> sprite_list);

        void draw_sprite(SpriteRenderer &sprite_renderer, const physics::Coords &coords);

        void update(float delta_time);

    private:
        unsigned int curent_sprite{0};
        std::vector<SpriteData> sprites;
        float fps{1.0f};
        float current_frame{0};
    };

}
