#pragma once
#include <glm/glm.hpp>
#include <texture_data.hpp>

namespace Graphic
{
struct SpriteData
{
    TextureData texture_data;
    glm::vec2 position{glm::vec2( 0, 0 )};
    glm::vec2 size{glm::vec2( 40, 40 )};
    float rotate{0.0f};
    glm::vec3 color{1.0f} ;
};

}
