#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <texture_data.hpp>

namespace Graphic
{

class TextureLoader
{
public:
    TextureLoader() = default;
    static TextureData generate_texture( std::string relative_path_to_texture, GLenum texture_format );
};

}
