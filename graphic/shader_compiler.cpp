#include "shader_compiler.hpp"
#include <glm/gtc/type_ptr.hpp>

namespace
{
std::string read_file ( const std::string& path )
{
    std::ifstream ifs ( path );
    std::string shaderSource ( ( std::istreambuf_iterator<char> ( ifs ) ), ( std::istreambuf_iterator<char>() ) );
    return shaderSource;
}

}
namespace Graphic
{

ShaderCompiler::ShaderCompiler ( std::string vertex_shader_path, std::string fragment_shader_path )
    :  vertex_shader_source ( read_file ( vertex_shader_path ) ),
       fragment_shader_source ( read_file ( fragment_shader_path ) )
{

}

void ShaderCompiler::link_shaders()
{
    shader_id =  glCreateProgram();
    glAttachShader ( shader_id, vertex_shader );
    glAttachShader ( shader_id, fragment_shader );
    glLinkProgram ( shader_id );
    int  success;
    glGetProgramiv ( shader_id, GL_LINK_STATUS, &success );
    if ( !success )
    {
        char infoLog[512];
        glGetProgramInfoLog ( shader_id, 512, NULL, infoLog );
        std::cerr << "ERROR::SHADER::LINKING::FAILED\n" << infoLog << std::endl;
        throw std::runtime_error ( "ERROR::SHADER::PROGRAM::LINKING_FAILED" );
    }
}
void ShaderCompiler::compile_vertex_shader()
{
    vertex_shader =  glCreateShader ( GL_VERTEX_SHADER );
    const auto vertex_shader_code = vertex_shader_source.c_str();
    glShaderSource ( vertex_shader, 1, &vertex_shader_code, NULL );
    glCompileShader ( vertex_shader );
    int  success;
    glGetShaderiv ( vertex_shader, GL_COMPILE_STATUS, &success );
    if ( !success )
    {
        char infoLog[512];
        glGetShaderInfoLog ( vertex_shader, 512, NULL, infoLog );
        std::cerr <<  infoLog << std::endl;
        throw std::runtime_error ( "ERROR::SHADER::VERTEX::COMPILATION_FAILED" );
    }
}
void ShaderCompiler::compile_fragment_shader()
{
    fragment_shader =  glCreateShader ( GL_FRAGMENT_SHADER );
    const auto fragment_shader_code = fragment_shader_source.c_str();
    glShaderSource ( fragment_shader, 1, &fragment_shader_code, NULL );
    glCompileShader ( fragment_shader );
    int  success;
    glGetShaderiv ( fragment_shader, GL_COMPILE_STATUS, &success );
    if ( !success )
    {
        char infoLog[512];
        glGetShaderInfoLog ( fragment_shader, 512, NULL, infoLog );
        std::cerr << infoLog << std::endl;
        throw std::runtime_error ( "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED" );
    }
}

Shader ShaderCompiler::compile_shaders()
{
    compile_vertex_shader();
    compile_fragment_shader();
    link_shaders();
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    return Shader{shader_id};
}

}
