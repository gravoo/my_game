#pragma once
#include <glm/glm.hpp>

namespace input_components
{

class IInputComponent
{
public:
    virtual glm::vec2 update() = 0;
    virtual ~IInputComponent() = default;
};
}
