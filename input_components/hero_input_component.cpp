#include "hero_input_component.hpp"
#include <GLFW/glfw3.h>

namespace input_components
{
glm::vec2 HeroInputComponent::update()
{
    auto keys = input_handler.get_key_array();
    glm::vec2 velocity{0};
    if( keys[GLFW_KEY_W] )
    {
        velocity.y = -1;
        velocity = glm::normalize(velocity);
    }
    else if( keys[GLFW_KEY_S] )
    {
        velocity.y = 1;
        velocity = glm::normalize(velocity);
    }
    if( keys[GLFW_KEY_D] )
    {
        velocity.x = 1;
        velocity = glm::normalize(velocity);
    }
    else if( keys[GLFW_KEY_A] )
    {
        velocity.x = -1;
        velocity = glm::normalize(velocity);
    }
    return velocity;
}


HeroInputComponent::HeroInputComponent( user_interface::InputHandler& input_handler )
    : input_handler( input_handler ) {}


}

