#include "game_menu_input_component.hpp"
#include <GLFW/glfw3.h>

input_components::GameMenuInputComponent::GameMenuInputComponent(user_interface::InputHandler &input_handler)
        : my_input_handler(input_handler)
{}

input_components::MenuInput input_components::GameMenuInputComponent::update()
{
    auto keys = my_input_handler.get_key_array();
    if (keys[GLFW_KEY_ESCAPE])
    {
        return MenuInput::END;
    }
    return MenuInput::NONE;
}
