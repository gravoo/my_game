cmake_minimum_required(VERSION 2.8.2)
add_compile_options(-Wall -Wextra -Wpedantic)
set(SOURCE hero_input_component.cpp i_input_component.hpp hero_attack_component.cpp main_menu_input_component.cpp game_menu_input_component.cpp game_menu_input_component.hpp)

add_library(input_components STATIC ${SOURCE})
target_link_libraries(input_components PRIVATE graphic user_interface)
target_include_directories(input_components PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
