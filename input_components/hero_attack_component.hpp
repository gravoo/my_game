#pragma once
#include <i_input_component.hpp>
#include <input_handler.hpp>
namespace input_components  {

class  HeroAttackComponent : public IInputComponent
{
public:
    HeroAttackComponent( user_interface::InputHandler& input_handler );
    glm::vec2 update() override;
private:
    user_interface::InputHandler& input_handler;

};

}

