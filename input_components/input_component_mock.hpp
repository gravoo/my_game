#pragma once

#include "gmock/gmock.h"
#include "i_input_component.hpp"

class InputComponentMock : public IInputComponent
{
public:
    MOCK_METHOD1(update, glm::vec2 (const int const_speed));
};
