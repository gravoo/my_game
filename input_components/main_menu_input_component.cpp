#include <main_menu_input_component.hpp>
#include <input_handler.hpp>
#include <GLFW/glfw3.h>
namespace input_components
{
MenuInputComponent::MenuInputComponent(user_interface::InputHandler& input_handler)
    : my_input_handler(input_handler) {}


MenuInput MenuInputComponent::update()
{
    auto keys = my_input_handler.get_key_array();
    if(keys[GLFW_KEY_UP])
    {
        return MenuInput::UP;
    }
    else if(keys[GLFW_KEY_DOWN])
    {
        return MenuInput::DOWN;
    }
    else if(keys[GLFW_KEY_ENTER])
    {
        return MenuInput::ENTER;
    }

    return MenuInput::NONE;
}

}

