#pragma once

namespace user_interface
{
    class InputHandler;
}

namespace input_components
{
    enum class MenuInput
    {
        UP, DOWN, ENTER, NONE, END
    };

    class MenuInputComponent
    {
    public:
        MenuInputComponent(user_interface::InputHandler &input_handler);

        MenuInput update();

    private:
        user_interface::InputHandler &my_input_handler;
    };
}
