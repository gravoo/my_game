#pragma once

#include <input_handler.hpp>
#include "main_menu_input_component.hpp"

namespace input_components
{
    class GameMenuInputComponent
    {
    public:
        GameMenuInputComponent(user_interface::InputHandler &input_handler);

        MenuInput update();

    private:
        user_interface::InputHandler &my_input_handler;
    };
}