#include "hero_attack_component.hpp"
#include <GLFW/glfw3.h>
#include <iostream>
using namespace input_components ;

glm::vec2  HeroAttackComponent::update()
{
    auto keys = input_handler.get_key_array();
    glm::vec2 velocity{0};
    if( keys[GLFW_KEY_UP] )
    {
        velocity.y = -1;
        velocity = glm::normalize(velocity);
    }
    else if( keys[GLFW_KEY_DOWN] )
    {
        velocity.y = 1;
        velocity = glm::normalize(velocity);
    }
    if( keys[GLFW_KEY_RIGHT] )
    {
        velocity.x = 1;
        velocity = glm::normalize(velocity);
    }
    else if( keys[GLFW_KEY_LEFT] )
    {
        velocity.x = -1;
        velocity = glm::normalize(velocity);
    }
    return velocity;
}

HeroAttackComponent::HeroAttackComponent( user_interface::InputHandler& input_handler )
    : input_handler( input_handler ) {}
