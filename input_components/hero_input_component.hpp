#pragma once
#include <input_handler.hpp>
#include <glad/glad.h>
#include <i_input_component.hpp>

namespace input_components
{
class HeroInputComponent : public IInputComponent
{
public:
    HeroInputComponent(user_interface::InputHandler& input_handler);
    glm::vec2 update() override;
private:
    user_interface::InputHandler& input_handler;
};
}
