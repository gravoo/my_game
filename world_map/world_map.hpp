#pragma once
#include <sprite_renderer.hpp>
#include <sprite_data.hpp>
#include <vector>

namespace game_world
{
class WorldMap
{
public:
    WorldMap(int widht = 800, int height = 600);
    void draw(Graphic::SpriteRenderer & sprite_renderer);
    void build_simple_map();
private:
    void build_tiles_row(int position_y);
    void build_tiled_map();
    Graphic::SpriteData world_template;
    std::vector<Graphic::SpriteData> tiles;
    int map_widht{800};
    int map_height{600};

};

}

