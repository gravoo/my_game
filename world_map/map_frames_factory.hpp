#pragma once

#include <memory>
#include <unmovable_hero.hpp>
#include <i_rectangle_colision_component.hpp>

namespace my_hero
{
    class IHero;
}
namespace game_world
{

    class MapFramesFactory
    {
    public:
        MapFramesFactory() = default;

        void create_and_insert_unmovable_hero(std::initializer_list<Graphic::SpriteData> sprites,
                                              physics::Coords my_coordinates,
                                              std::set<std::unique_ptr<my_hero::IHero>> &heros);
    };
}


