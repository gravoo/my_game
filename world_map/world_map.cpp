#include "world_map.hpp"
#include "sprite_factory.hpp"

namespace game_world
{
void WorldMap::draw( Graphic::SpriteRenderer& sprite_renderer )
{
    for( const auto& tile : tiles )
    {
        sprite_renderer.draw_sprite( tile );
    }

}

void WorldMap::build_tiles_row(int position_y)
{
    auto relative_position = glm::vec2 {world_template.position.x, position_y};
    int num_of_tiles = map_widht / world_template.size.x;

    for( auto i {0}; i < num_of_tiles; ++i )
    {
        tiles.push_back( Graphic::SpriteData {world_template.texture_data, relative_position } );
        relative_position = glm::vec2( tiles.back().position.x + tiles.back().size.x, position_y );
    }
}
void WorldMap::build_simple_map()
{
    build_tiled_map();
}


void WorldMap::build_tiled_map()
{
    int num_of_rows = map_height / world_template.size.y;
    for(auto i{0}; i<num_of_rows; ++i)
    {
        build_tiles_row(world_template.position.y + i * world_template.size.y);
    }
}


WorldMap::WorldMap(int widht, int height) : map_widht(widht), map_height(height)
{
    world_template = Graphic::SpriteFactory {} .create_sprite( "../../textures/x_mark.png", GL_RGBA );
}

}

