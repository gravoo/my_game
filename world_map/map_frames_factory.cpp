#include "map_frames_factory.hpp"
#include <rectangle_colision_component.hpp>

void game_world::MapFramesFactory::create_and_insert_unmovable_hero(
        std::initializer_list<Graphic::SpriteData> sprites,
        physics::Coords my_coordinates, std::set<std::unique_ptr<my_hero::IHero>> &heros)
{
    heros.insert(std::make_unique<my_hero::UnMovableHero>(
            physics::HeroPhysicsComponent{std::make_unique<physics::RectangleColisionComponent>(heros)},
            my_coordinates, Graphic::SpriteAnimator{sprites}));
}