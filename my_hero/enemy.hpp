#pragma once
#include "i_hero.hpp"
#include <hero_physics_component.hpp>
#include <ai_component.hpp>
#include <memory>
#include <coordinate_data.hpp>
#include <sprite_animator.hpp>
#include <health_component.hpp>

namespace Graphic
{
struct SpriteData;
class SpriteRenderer;
}

namespace my_hero
{

class Enemy :  public IHero
{
public:
    Enemy( physics::HeroPhysicsComponent physics_component,
           const glm::vec2 &protagonist_position,
           physics::Coords coordinates,
           Graphic::SpriteAnimator sprite_animator,
           my_enemy::AiComponent ai_component,
           int speed );
    void update(float delta_time) override;
    void draw( Graphic::SpriteRenderer& sprite_renderer ) override;
    physics::Coords get_coordinate() const override;
    HeroState get_hero_state() override;
    void receive_damage(unsigned damage, const glm::vec2& hit_direction) override;
private:
    physics::HeroPhysicsComponent physics_component;
    const glm::vec2 &protagonist_position;
    my_enemy::AiComponent ai_component;
    physics::Coords my_coordinates;
    Graphic::SpriteAnimator sprite_animator;
    int speed;
    HeroState state = HeroState::active;
    glm::vec2 damage_direction {0};
    HealthComponent health_component;
};

}
