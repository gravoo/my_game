#include <arrow_hero.hpp>
#include <i_physics_component.hpp>

namespace my_hero
{
void ArrowHero::update(float delta_time)
{
    physics_component->update_position(my_coords, velocity);
    if(physics_component->colision_detected())
    {
        state = HeroState::inactive;
    }
    sprite_animator.update(delta_time);
}

void ArrowHero::draw(Graphic::SpriteRenderer& sprite_renderer)
{
    sprite_animator.draw_sprite(sprite_renderer, my_coords);
}

physics::Coords ArrowHero::get_coordinate() const
{
    return my_coords;
}


ArrowHero::ArrowHero(std::unique_ptr< physics::IPhysicsComponent > physics_component,
                     physics::Coords my_coords, Graphic::SpriteAnimator sprite_animator,
                     glm::vec2 velocity)
    : physics_component(std::move(physics_component)),
      my_coords(my_coords),
      sprite_animator(sprite_animator),
      velocity(velocity){}


HeroState ArrowHero::get_hero_state()
{
    return state;
}

}

