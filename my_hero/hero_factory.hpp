#pragma once

#include <memory>
#include <string>
#include <hero.hpp>
#include <enemy.hpp>
#include <unmovable_hero.hpp>
#include <set>
#include <i_hero.hpp>
#include <coordinate_data.hpp>
#include <functional>
#include <i_rectangle_colision_component.hpp>

namespace user_interface
{
    class Camera;

    class InputHandler;
}

namespace my_hero
{

    class HeroFactory
    {
    public:
        HeroFactory(user_interface::Camera &camera, user_interface::InputHandler &input_handler);

        void create_and_insert_hero(
                std::shared_ptr<physics::Coords> main_character,
                std::initializer_list<Graphic::SpriteData> sprites,
                std::set<std::unique_ptr<my_hero::IHero>> &heros);

    private:
        user_interface::Camera &camera;
        user_interface::InputHandler &input_handler;

    };
}
