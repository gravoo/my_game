#pragma once
#include <sprite_data.hpp>

namespace physics
{
struct Coords;
}
namespace Graphic
{
class SpriteRenderer;
}
namespace my_hero
{
class HealthComponent
{
public:
    HealthComponent(unsigned health, const physics::Coords &owner_coords);
    void update(const physics::Coords &owner_coords);
    void draw(Graphic::SpriteRenderer& sprite_renderer);
    void receive_damage(unsigned int damage);
    bool is_health_empty();
private:
    unsigned my_health{0};
    Graphic::SpriteData my_sprite;
};
}
