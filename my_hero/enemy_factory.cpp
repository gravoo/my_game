
#include <rectangle_colision_component.hpp>
#include <hero_factory.hpp>
#include "enemy_factory.hpp"

namespace my_hero
{
    void EnemyFactory::create_and_insert_enemy(std::initializer_list<Graphic::SpriteData> sprites, physics::Coords enemy_coordinates,
                                               int velocity, std::set<std::unique_ptr<IHero>> &heros, std::shared_ptr<physics::Coords> main_character)
    {
        heros.insert(std::make_unique<my_hero::Enemy>(
                physics::HeroPhysicsComponent{std::make_unique<physics::RectangleColisionComponent>(heros)},
                main_character->position,
                enemy_coordinates,
                Graphic::SpriteAnimator{sprites},
                my_enemy::AiComponent{200, ArrowComponent{ArrowFactory{heros}}},
                velocity));
    }
}