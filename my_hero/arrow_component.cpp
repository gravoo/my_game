#include <arrow_component.hpp>
#include <sprite_factory.hpp>
#include <arrow_hero.hpp>
#include <sprite_animator.hpp>
#include <rectangle_colision_component.hpp>
#include <hero_physics_component.hpp>
#include <i_hero.hpp>
#include <sprite_renderer.hpp>

namespace my_hero
{
ArrowComponent:: ArrowComponent(ArrowFactory arrow_factory)
    : arrow_factory(arrow_factory)
{
}

void ArrowComponent::update(float delta_time, glm::vec2 direction,
                            physics::Coords hero_coords, glm::vec2 hero_velocity)
{
    reloading_time += delta_time;

    add_new_arrow(direction, hero_coords, hero_velocity);

    for(auto const& arrow : arrows)
    {
        arrow->update(delta_time);
        if(arrow->get_hero_state() == HeroState::inactive)
        {
            arrows.erase(arrow);
        }
    }
}

void ArrowComponent::create_fast_arrow(glm::vec2 velocity)
{
    my_coordinates = prepare_arrow_coords();
    arrows.insert(arrow_factory.create_arrow(my_coordinates, velocity * move_direction));
}


bool ArrowComponent::is_arrow_moving(glm::vec2 arrow_movement_direction)
{
    if(arrow_movement_direction.x != 0 || arrow_movement_direction.y != 0)
    {
        return true;
    }

    return false;
}

bool ArrowComponent::bow_reloaded()
{
    if(reloading_time > 0.25f)
    {
        return true;
    }

    return false;
}


physics::Coords ArrowComponent::prepare_arrow_coords()
{
    if(move_direction.x > 0.25f)
    {
        my_coordinates.position.x += (my_coordinates.size.x + 5);
    }
    else if(move_direction.x < -0.25f)
    {
        my_coordinates.position.x -= (my_coordinates.size.x + 5);
    }

    if(move_direction.y > 0.25f)
    {
        my_coordinates.position.y += (my_coordinates.size.y + 5);
    }
    else if(move_direction.y < -0.25f)
    {
        my_coordinates.position.y -= (my_coordinates.size.y + 5);
    }

    my_coordinates.size = glm::vec2(30, 30);
    return my_coordinates;
}


void ArrowComponent::draw_arrows(Graphic::SpriteRenderer& sprite_renderer)
{
    for(auto const& arrow : arrows)
    {
        arrow->draw(sprite_renderer);
    }
}


void ArrowComponent::add_new_arrow(glm::vec2 direction, physics::Coords hero_coords, glm::vec2 hero_velocity)
{
    if(is_arrow_moving(direction) && bow_reloaded())
    {
        my_coordinates = hero_coords;
        move_direction = direction;
        reloading_time = 0.0f;
        create_fast_arrow(default_velocity + hero_velocity);
    }
}

}

