#pragma once
#include <set>
#include <memory>
#include <sprite_data.hpp>
#include <glm/glm.hpp>
#include <coordinate_data.hpp>
#include <string>
#include <map>

namespace my_hero
{
class IHero;
class ArrowFactory
{
public:
    ArrowFactory(std::set<std::unique_ptr<my_hero::IHero>>& heros);
    std::unique_ptr<my_hero::IHero> create_arrow( physics::Coords my_coordinates, glm::vec2 velocity);
private:
    std::map<std::string, Graphic::SpriteData > sprites;
    std::set<std::unique_ptr<my_hero::IHero>>& heros;

};
}
