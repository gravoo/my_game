#pragma once
#include <sprite_data.hpp>
#include <glm/glm.hpp>
#include <coordinate_data.hpp>
#include <arrow_factory.hpp>
#include <set>
#include <memory>

namespace Graphic
{
class SpriteRenderer;
}
namespace my_hero
{
class IHero;
class ArrowComponent
{
public:
    ArrowComponent(ArrowFactory arrow_factory);
    void update(float delta_time, glm::vec2 direction, physics::Coords hero_coords, glm::vec2 hero_velocity);
    void draw_arrows(Graphic::SpriteRenderer& sprite_renderer);
private:
    void add_new_arrow(glm::vec2 direction, physics::Coords hero_coords, glm::vec2 hero_velocity);
    void create_fast_arrow(glm::vec2 velocity);
    bool is_arrow_moving(glm::vec2 arrow_movement_direction);
    bool bow_reloaded();
    physics::Coords prepare_arrow_coords();
    float reloading_time {0.0f};
    glm::vec2 move_direction {0};
    glm::vec2 default_velocity{5, 5};
    physics::Coords my_coordinates;
    ArrowFactory arrow_factory;
    std::set<std::unique_ptr<my_hero::IHero>> arrows;

};
}
