#pragma once

#include <memory>
#include <hero.hpp>
#include <enemy.hpp>
#include <unmovable_hero.hpp>
#include <i_rectangle_colision_component.hpp>

namespace my_hero
{
    class EnemyFactory
    {

    public:
        void create_and_insert_enemy(
                std::initializer_list<Graphic::SpriteData> sprites,
                physics::Coords enemy_coordinates,
                int velocity,
                std::set<std::unique_ptr<my_hero::IHero>> &heros,
                std::shared_ptr<physics::Coords> main_character);
    };

}
