#include <health_component.hpp>
#include <sprite_factory.hpp>
#include <coordinate_data.hpp>
#include <sprite_renderer.hpp>
#include <glm/glm.hpp>
#include <iostream>

namespace my_hero
{
HealthComponent::HealthComponent(unsigned int health, const physics::Coords &owner_coords)
    : my_health(health),
      my_sprite(Graphic::SpriteFactory(). create_sprite(
                    "../../textures/health_bar.png", GL_RGBA,
                    glm::vec2(owner_coords.position.x, owner_coords.get_y_dimension()),
                    glm::vec2(owner_coords.size.x, owner_coords.size.y - 25 )))
{

}

void HealthComponent::draw(Graphic::SpriteRenderer& sprite_renderer)
{
    sprite_renderer.draw_sprite(my_sprite);
}


void HealthComponent::update(const physics::Coords &owner_coords)
{
    my_sprite.position = glm::vec2(owner_coords.position.x, owner_coords.get_y_dimension());
}


bool HealthComponent::is_health_empty()
{
    return my_health > 0 ? false : true;
}

void HealthComponent::receive_damage(unsigned int damage)
{
    auto percent_damage_received = static_cast<float>(damage)/static_cast<float>(my_health);
    my_health -= damage;
    my_sprite.size.x -= my_sprite.size.x * percent_damage_received;
}

}

