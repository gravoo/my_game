#pragma once

#include <glm/glm.hpp>
#include <sprite_data.hpp>
#include <coordinate_data.hpp>

namespace Graphic
{
    class SpriteRenderer;
}
namespace my_hero
{
    enum class HeroState
    {
        active, inactive, end
    };

    class IHero
    {
    public:
        virtual ~IHero() = default;

        virtual void update(float delta_time) = 0;

        virtual void draw(Graphic::SpriteRenderer &sprite_renderer) = 0;

        virtual HeroState get_hero_state() = 0;

        virtual physics::Coords get_coordinate() const = 0;

        virtual void receive_damage(unsigned damage, const glm::vec2 &hit_direction) = 0;
    };
}
