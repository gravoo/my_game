#include <unmovable_hero.hpp>
#include <i_rectangle_colision_component.hpp>

namespace my_hero
{
void UnMovableHero::update(float delta_time)
{
    sprite_animator.update(delta_time);
}

void UnMovableHero::draw(Graphic::SpriteRenderer& sprite_renderer)
{
    sprite_animator.draw_sprite(sprite_renderer, my_coordinates);
}

physics::Coords UnMovableHero::get_coordinate() const
{
    return my_coordinates;
}

UnMovableHero::UnMovableHero(physics::HeroPhysicsComponent physics_component,
                             physics::Coords my_coordinates,
                             Graphic::SpriteAnimator sprite_animator)
    : physics_component(std::move(physics_component)),
      my_coordinates(my_coordinates),
      sprite_animator(sprite_animator) {}


HeroState UnMovableHero::get_hero_state()
{
    return state;
}


void UnMovableHero::receive_damage(unsigned int damage, const glm::vec2& )
{
    hp -= damage;
}

}

