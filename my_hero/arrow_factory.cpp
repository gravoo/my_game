#include <arrow_factory.hpp>
#include <sprite_factory.hpp>
#include <sprite_animator.hpp>
#include <i_hero.hpp>
#include <hero_physics_component.hpp>
#include <rectangle_colision_component.hpp>
#include <arrow_hero.hpp>
#include <arrow_physics_component.hpp>

namespace my_hero
{
std::unique_ptr<my_hero::IHero>ArrowFactory::create_arrow(physics::Coords my_coordinates, glm::vec2 velocity)
{
    return std::make_unique<ArrowHero> (
                         std::make_unique<physics::ArrowPhysicsComponent>(
                             std::make_unique<physics::RectangleColisionComponent>(heros), heros),
                         my_coordinates, Graphic::SpriteAnimator {
                             sprites["fire_ball_1"], sprites["fire_ball_2"],
                             sprites["fire_ball_3"], sprites["fire_ball_4"]}, velocity);
}

ArrowFactory::ArrowFactory(std::set< std::unique_ptr< my_hero::IHero > >& heros)
    : heros(heros)
{
    sprites["fire_ball_1"] = Graphic::SpriteFactory().create_sprite("../../textures/fire_ball_1.png", GL_RGBA);
    sprites["fire_ball_2"] = Graphic::SpriteFactory().create_sprite("../../textures/fire_ball_2.png", GL_RGBA);
    sprites["fire_ball_3"] = Graphic::SpriteFactory().create_sprite("../../textures/fire_ball_3.png", GL_RGBA);
    sprites["fire_ball_4"] = Graphic::SpriteFactory().create_sprite("../../textures/fire_ball_4.png", GL_RGBA);
}

}

