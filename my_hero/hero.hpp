#pragma once

#include <sprite_data.hpp>
#include <sprite_renderer.hpp>
#include <i_input_component.hpp>
#include <hero_physics_component.hpp>
#include "i_hero.hpp"
#include <camera_position_update.hpp>
#include <memory>
#include <sprite_animator.hpp>
#include <coordinate_data.hpp>
#include <arrow_component.hpp>
#include <set>
#include <health_component.hpp>
#include <game_menu_input_component.hpp>

namespace my_hero
{
    class Hero : public IHero
    {
    public:
        Hero(std::unique_ptr<input_components::IInputComponent> move_component, std::unique_ptr<input_components::IInputComponent> attack_component,
             std::unique_ptr<input_components::GameMenuInputComponent> menu_component, physics::HeroPhysicsComponent physics_component,
             user_interface::CameraPositionUpdate camera_update_position, Graphic::SpriteAnimator sprite_animator,
             std::shared_ptr<physics::Coords> coordinate_data, ArrowComponent arrow_component, int speed = 3);

        void update(float delta_time) override;

        void draw(Graphic::SpriteRenderer &sprite_renderer) override;

        physics::Coords get_coordinate() const override;

        HeroState get_hero_state() override;

        void receive_damage(unsigned damage, const glm::vec2 &hit_direction) override;

    private:
        glm::vec2 velocity{4, 4};
        std::unique_ptr<input_components::IInputComponent> move_component;
        std::unique_ptr<input_components::IInputComponent> attack_component;
        std::unique_ptr<input_components::GameMenuInputComponent> menu_component;
        physics::HeroPhysicsComponent physics_component;
        user_interface::CameraPositionUpdate camera_update_position;
        Graphic::SpriteAnimator sprite_animator;
        std::shared_ptr<physics::Coords> my_coordinates;
        ArrowComponent arrow_component;
        const int speed{1};
        HeroState state = HeroState::active;
        glm::vec2 damage_direction{0};
        HealthComponent health_component;
    };
}

