#pragma once
#include <i_hero.hpp>
#include <coordinate_data.hpp>
#include <sprite_animator.hpp>
#include <sprite_renderer.hpp>
#include <hero_physics_component.hpp>
#include <memory>

namespace my_hero
{
class UnMovableHero : public IHero
{
public:
    UnMovableHero(physics::HeroPhysicsComponent physics_component,
                  physics::Coords my_coordinates,
                  Graphic::SpriteAnimator sprite_animator);
    void update(float delta_time) override;
    void draw(Graphic::SpriteRenderer& sprite_renderer) override;
    physics::Coords get_coordinate() const override;
    HeroState get_hero_state() override;
    void receive_damage(unsigned damage, const glm::vec2& ) override;
private:
    physics::HeroPhysicsComponent physics_component;
    physics::Coords my_coordinates;
    Graphic::SpriteAnimator sprite_animator;
    HeroState state = HeroState::active;
    unsigned hp {10000};
};
}
