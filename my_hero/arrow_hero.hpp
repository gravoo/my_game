#pragma once
#include "i_hero.hpp"
#include <sprite_animator.hpp>
#include <memory>

namespace physics
{
class IPhysicsComponent;
}

namespace my_hero
{
class ArrowHero : public IHero
{
public:
    ArrowHero(std::unique_ptr<physics::IPhysicsComponent> physics_component,
              physics::Coords my_coords,
              Graphic::SpriteAnimator sprite_animator,
              glm::vec2 velocity);
    void update(float delta_time) override;
    void draw(Graphic::SpriteRenderer& sprite_renderer) override;
    HeroState get_hero_state() override;
    void receive_damage(unsigned damage, const glm::vec2& hit_direction) override
    {
        hp -= damage;
    }
    physics::Coords get_coordinate() const override;
private:
    std::unique_ptr<physics::IPhysicsComponent> physics_component;
    physics::Coords my_coords;
    Graphic::SpriteAnimator sprite_animator;
    glm::vec2 velocity {0};
    HeroState state = HeroState::active;
    unsigned hp {10};
};
}

