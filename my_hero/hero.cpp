#include <hero.hpp>
#include <iostream>
#include <i_rectangle_colision_component.hpp>
#include <iostream>
#include <game_menu_input_component.hpp>

::std::ostream &operator<<(::std::ostream &os, const glm::vec2 &values)
{
    return os << "damage direction x: " << values.x << " y: " << values.y << "\n";
}

namespace my_hero
{
    void Hero::draw(Graphic::SpriteRenderer &sprite_renderer)
    {
        health_component.draw(sprite_renderer);
        sprite_animator.draw_sprite(sprite_renderer, *my_coordinates);
        arrow_component.draw_arrows(sprite_renderer);
    }

    void Hero::update(float delta_time)
    {
        auto hero_move_direction = move_component->update();
        auto arrow_move_direction = attack_component->update();
        auto last_position = my_coordinates->position;

        physics_component.update_position(my_coordinates, (velocity * hero_move_direction) + damage_direction);
        camera_update_position.update_camera_position(my_coordinates->position - last_position);
        arrow_component.update(delta_time, arrow_move_direction, *my_coordinates, velocity);
        sprite_animator.update(delta_time);
        damage_direction = glm::vec2{0};
        health_component.update(*my_coordinates);
        if (menu_component->update() == input_components::MenuInput::END)
        {
            state = HeroState::end;
        }
    }

    Hero::Hero(std::unique_ptr<input_components::IInputComponent> move_component, std::unique_ptr<input_components::IInputComponent> attack_component,
               std::unique_ptr<input_components::GameMenuInputComponent> menu_component, physics::HeroPhysicsComponent physics_component,
               user_interface::CameraPositionUpdate camera_update_position, Graphic::SpriteAnimator sprite_animator,
               std::shared_ptr<physics::Coords> coordinate_data, ArrowComponent arrow_component, int speed)
            : move_component(std::move(move_component)),
              attack_component(std::move(attack_component)),
              menu_component(std::move(menu_component)),
              physics_component(std::move(physics_component)),
              camera_update_position(camera_update_position),
              sprite_animator(sprite_animator),
              my_coordinates(coordinate_data),
              arrow_component(std::move(arrow_component)),
              speed(speed),
              health_component(500, *my_coordinates)
    {
    }

    physics::Coords Hero::get_coordinate() const
    {
        return *my_coordinates;
    }


    HeroState Hero::get_hero_state()
    {
        return state;
    }


    void Hero::receive_damage(unsigned int damage, const glm::vec2 &hit_direction)
    {
        damage_direction = glm::normalize(hit_direction);
        health_component.receive_damage(damage);
        if (health_component.is_health_empty())
        {
            state = HeroState::end;
            my_coordinates->position = glm::vec2(1000, 1000);
        }
    }

}

