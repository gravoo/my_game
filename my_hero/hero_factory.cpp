#include <hero_factory.hpp>
#include <hero_input_component.hpp>
#include <hero_attack_component.hpp>
#include <rectangle_colision_component.hpp>

namespace my_hero
{

    HeroFactory::HeroFactory(user_interface::Camera &camera, user_interface::InputHandler &input_handler)
            : camera(camera), input_handler(input_handler)
    {}


    void
    HeroFactory::create_and_insert_hero(std::shared_ptr<physics::Coords> main_character,
                                        std::initializer_list<Graphic::SpriteData> sprites,
                                        std::set<std::unique_ptr<my_hero::IHero>> &heros)
    {
        heros.insert(std::make_unique<Hero>(std::make_unique<input_components::HeroInputComponent>(input_handler),
                                            std::make_unique<input_components::HeroAttackComponent>(input_handler),
                                            std::make_unique<input_components::GameMenuInputComponent>(input_handler),
                                            physics::HeroPhysicsComponent{std::make_unique<physics::RectangleColisionComponent>(heros)},
                                            user_interface::CameraPositionUpdate(camera),
                                            Graphic::SpriteAnimator{sprites},
                                            main_character,
                                            ArrowComponent{ArrowFactory{heros}}));
    }

}
