#include "enemy.hpp"
#include <sprite_renderer.hpp>
#include <sprite_data.hpp>
#include <iostream>
#include <i_rectangle_colision_component.hpp>

namespace my_hero
{
Enemy::Enemy( physics::HeroPhysicsComponent physics_component,
             const glm::vec2& protagonist_position,
             physics::Coords coordinates,
             Graphic::SpriteAnimator sprite_animator,
             my_enemy::AiComponent ai_component,
             int speed)
    : physics_component(std::move(physics_component)),
      protagonist_position(protagonist_position),
      ai_component(std::move(ai_component)),
      my_coordinates(coordinates),
      sprite_animator(sprite_animator),
      speed(speed),
      health_component(100, my_coordinates)
{

}
void Enemy::draw(Graphic::SpriteRenderer& sprite_renderer)
{
    sprite_animator.draw_sprite(sprite_renderer, my_coordinates);
    ai_component.draw(sprite_renderer);
    health_component.draw(sprite_renderer);
}

void Enemy::update(float delta_time)
{
    glm::vec2 velocity {0};
    velocity = ai_component.udate(protagonist_position, my_coordinates, speed, delta_time);
    physics_component.update_position(my_coordinates, velocity + damage_direction);
    sprite_animator.update(delta_time);
    damage_direction = glm::vec2{0};
    health_component.update(my_coordinates);
}

physics::Coords Enemy::get_coordinate() const
{
    return my_coordinates;
}

HeroState Enemy::get_hero_state()
{
    return state;
}

void Enemy::receive_damage(unsigned int damage, const glm::vec2& hit_direction)
{
    health_component.receive_damage(damage);
    damage_direction = glm::normalize(hit_direction);
    if(health_component.is_health_empty())
    {
        state = HeroState::inactive;
    }
}

}

