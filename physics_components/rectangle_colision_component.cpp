#include <rectangle_colision_component.hpp>
#include <algorithm>

namespace physics
{
bool RectangleColisionComponent::colistion_detected(const Coords& hero_cords)
{
    auto colision_AABB = [&](const auto & hero)
    {
        return hero_cords != hero->get_coordinate()
               && hero_cords.get_x_dimension() >= hero->get_coordinate().position.x
               && hero->get_coordinate().get_x_dimension() >= hero_cords.position.x
               && hero_cords.get_y_dimension() >= hero->get_coordinate().position.y
               && hero->get_coordinate().get_y_dimension() >= hero_cords.position.y;
    };
    auto detected_sprite = std::find_if(heros.begin(), heros.end(), colision_AABB);
    return detected_sprite != heros.end();
}

bool RectangleColisionComponent::colistion_detected_shared(std::shared_ptr<Coords> hero_cords)
{
    auto colision_AABB = [&](const auto & hero)
    {
        return *hero_cords != hero->get_coordinate()
               && hero_cords->get_x_dimension() >= hero->get_coordinate().position.x
               && hero->get_coordinate().get_x_dimension() >= hero_cords->position.x
               && hero_cords->get_y_dimension() >= hero->get_coordinate().position.y
               && hero->get_coordinate().get_y_dimension() >= hero_cords->position.y;
    };
    auto detected_sprite = std::find_if(heros.begin(), heros.end(), colision_AABB);
    return detected_sprite != heros.end();
}

RectangleColisionComponent::RectangleColisionComponent(
    const std::set< std::unique_ptr< my_hero::IHero > >& heros)
    : heros(heros) {}


std::set<std::unique_ptr<my_hero::IHero>>::iterator RectangleColisionComponent::colided_element(
    const Coords& hero_cords)
{
        auto colision_AABB = [&](const auto & hero)
    {
        return hero_cords != hero->get_coordinate()
               && hero_cords.get_x_dimension() >= hero->get_coordinate().position.x
               && hero->get_coordinate().get_x_dimension() >= hero_cords.position.x
               && hero_cords.get_y_dimension() >= hero->get_coordinate().position.y
               && hero->get_coordinate().get_y_dimension() >= hero_cords.position.y;
    };
    auto detected_sprite = std::find_if(heros.begin(), heros.end(), colision_AABB);
    return detected_sprite;
}

}

