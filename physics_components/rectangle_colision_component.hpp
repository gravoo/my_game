#pragma once
#include <set>
#include <memory>
#include <i_hero.hpp>
#include <i_rectangle_colision_component.hpp>

namespace physics
{
class RectangleColisionComponent : public IRectangleColisionComponent
{
public:
    RectangleColisionComponent(const std::set<std::unique_ptr<my_hero::IHero>>& heros);
    bool colistion_detected(const Coords& hero_cords) override;
    bool colistion_detected_shared(std::shared_ptr<Coords> hero_cords) override;
    std::set<std::unique_ptr<my_hero::IHero>>::iterator colided_element(const Coords& hero_cords) override;
private:
    const std::set<std::unique_ptr<my_hero::IHero>>& heros;
};
}
