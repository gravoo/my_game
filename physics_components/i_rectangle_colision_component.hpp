#pragma once
#include <coordinate_data.hpp>
#include <set>
#include <memory>

namespace my_hero
{
    class IHero;
}
namespace physics
{
class IRectangleColisionComponent
{
public:
    virtual bool colistion_detected(const Coords& hero_cords) = 0;
    virtual bool colistion_detected_shared(std::shared_ptr<Coords> hero_cords) = 0;
    virtual std::set<std::unique_ptr<my_hero::IHero>>::iterator colided_element(const Coords& hero_cords) = 0;
    virtual ~IRectangleColisionComponent() = default;
};
}
