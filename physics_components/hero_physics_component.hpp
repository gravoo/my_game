#pragma once
#include <memory>
#include <i_physics_component.hpp>

namespace physics
{
class IRectangleColisionComponent;

class HeroPhysicsComponent : public IPhysicsComponent
{
public:
    HeroPhysicsComponent(std::unique_ptr<IRectangleColisionComponent> colision_component);
    void update_position( Coords &hero_cords, const glm::vec2& velocity ) override;
    void update_position( std::shared_ptr<Coords> hero_cords, const glm::vec2& velocity ) override;
    bool colision_detected() override;
private:
    std::unique_ptr<IRectangleColisionComponent> colision_component;
    bool colided{false};
};
}
