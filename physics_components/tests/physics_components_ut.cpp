#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <hero_physics_component.hpp>
#include <rectangle_colision_component_mock.hpp>

using namespace testing;
using namespace physics;

struct enemy_input_component_ut : public Test
{
    physics::Coords testable_coords{glm::vec2{0, 0},  glm::vec2(40, 40)};
    glm::vec2 velocity {1, 1};
    std::unique_ptr<NiceMock<RectangleColisionComponentMock>> colision_component_mock = std::make_unique<NiceMock<RectangleColisionComponentMock>>();
    NiceMock<RectangleColisionComponentMock> * mock_ptr;
    std::unique_ptr<HeroPhysicsComponent> physics_component_cut;
    enemy_input_component_ut()
    {
        mock_ptr = colision_component_mock.get();
        physics_component_cut = std::make_unique<HeroPhysicsComponent>(std::move(colision_component_mock));
    }

};

TEST_F(enemy_input_component_ut, do_not_update_position_if_colistion_detected)
{
    EXPECT_CALL(*mock_ptr, colistion_detected(_) ).WillOnce(Return(true));
    physics_component_cut->update_position(testable_coords, velocity);
    ASSERT_EQ(glm::vec2(0, 0), testable_coords.position);
}

TEST_F(enemy_input_component_ut, update_position_if_colistion_not_detected)
{
    EXPECT_CALL(*mock_ptr, colistion_detected(_) ).WillOnce(Return(false));
    physics_component_cut->update_position(testable_coords, velocity);
    ASSERT_EQ(glm::vec2(0, 0) + velocity, testable_coords.position);
}
