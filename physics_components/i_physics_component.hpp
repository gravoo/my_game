#pragma once
#include <coordinate_data.hpp>
#include <memory>

namespace physics
{
class IPhysicsComponent
{
public:
    virtual void update_position(Coords& hero_cords, const glm::vec2& velocity) = 0;
    virtual void update_position( std::shared_ptr<Coords> hero_cords, const glm::vec2& velocity ) = 0;
    virtual bool colision_detected() = 0;
    virtual ~IPhysicsComponent() = default;
};
}
