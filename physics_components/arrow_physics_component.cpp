#include <arrow_physics_component.hpp>
#include <i_rectangle_colision_component.hpp>
#include <iostream>
#include <i_hero.hpp>

namespace physics
{
void ArrowPhysicsComponent::update_position(Coords& hero_cords, const glm::vec2& velocity)
{
    hero_cords.position += velocity;
    auto hited_element = colision_component->colided_element(hero_cords);

    if(hited_element != heros.end())
    {
        colided = true;
        (*hited_element)->receive_damage(10, velocity);
    }
}


ArrowPhysicsComponent::ArrowPhysicsComponent(std::unique_ptr< IRectangleColisionComponent > colision_component,
                                             std::set<std::unique_ptr<my_hero::IHero>>& heros
                                            )
    : colision_component(std::move(colision_component)), heros(heros) {}


bool ArrowPhysicsComponent::colision_detected()
{
    return colided;
}

}

