#include "hero_physics_component.hpp"
#include <i_rectangle_colision_component.hpp>
#include <i_hero.hpp>

namespace physics
{
HeroPhysicsComponent::HeroPhysicsComponent(std::unique_ptr<IRectangleColisionComponent> colision_component)
    : colision_component(std::move(colision_component)) {}

void HeroPhysicsComponent::update_position( Coords &hero_cords, const glm::vec2& velocity )
{
    hero_cords.position += velocity;

    if( colision_component->colistion_detected( hero_cords ) )
    {
        hero_cords.position -= velocity;
    }
}

void HeroPhysicsComponent::update_position( std::shared_ptr<Coords> hero_cords, const glm::vec2& velocity )
{
    hero_cords->position += velocity;

    if( colision_component->colistion_detected_shared( hero_cords ) )
    {
        hero_cords->position -= velocity;
    }
}


bool HeroPhysicsComponent::colision_detected()
{
    return colided;
}

}

