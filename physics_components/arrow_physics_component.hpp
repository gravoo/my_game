#pragma once
#include <i_physics_component.hpp>
#include <memory>
#include <set>
namespace my_hero
{
class IHero;
}
namespace physics
{
class IRectangleColisionComponent;

class ArrowPhysicsComponent : public IPhysicsComponent
{
public:
    ArrowPhysicsComponent(std::unique_ptr<IRectangleColisionComponent> colision_component,
                          std::set<std::unique_ptr<my_hero::IHero>>& heros );
    void update_position(Coords& hero_cords, const glm::vec2& velocity) override;
    void update_position( std::shared_ptr<Coords> hero_cords, const glm::vec2& velocity ) override
    {}
    bool colision_detected() override;
private:
    std::unique_ptr<IRectangleColisionComponent> colision_component;
    std::set<std::unique_ptr<my_hero::IHero>>& heros;
    bool colided{false};
};
}
