#pragma once

#include "gmock/gmock.h"
#include "i_rectangle_colision_component.hpp"
namespace physics
{
class RectangleColisionComponentMock : public IRectangleColisionComponent
{
public:
    MOCK_METHOD1(colistion_detected, bool (const Coords& hero_cords));
    MOCK_METHOD1(colistion_detected_shared, bool (std::shared_ptr<Coords> hero_cords));
    MOCK_METHOD1(colided_element, std::set<std::unique_ptr<my_hero::IHero>>::iterator(const Coords& hero_cords));
};

}
