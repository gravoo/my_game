cmake_minimum_required(VERSION 2.8.2)
add_compile_options(-Wall -Wextra -Wpedantic)
set(SOURCE i_rectangle_colision_component.hpp hero_physics_component.cpp rectangle_colision_component.cpp arrow_physics_component.cpp)

add_subdirectory(tests)
add_library(physics_components STATIC ${SOURCE})
target_link_libraries(physics_components PRIVATE graphic my_hero glm glad)
target_include_directories(physics_components PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
